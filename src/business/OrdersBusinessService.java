package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import Models.Order;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	
	private List<Order> orders = new ArrayList<Order>();

    /**
     * Default constructor. 
     */
    public OrdersBusinessService() {
    	orders.add(new Order("0000001", "This is product 1", (float)1.00, 1));
		orders.add(new Order("0000002", "This is product 2", (float)2.00, 2));
		orders.add(new Order("0000003", "This is product 3", (float)3.00, 3));
		orders.add(new Order("0000004", "This is product 4", (float)4.00, 4));
		orders.add(new Order("0000005", "This is product 5", (float)5.00, 5));
		orders.add(new Order("0000006", "This is product 6", (float)6.00, 6));
		orders.add(new Order("0000007", "This is product 7", (float)7.00, 7));
		orders.add(new Order("0000008", "This is product 8", (float)8.00, 8));
		orders.add(new Order("0000009", "This is product 9", (float)9.00, 9));
		orders.add(new Order("00000010", "This is product 10", (float)10.00, 10));
    }
    
    

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        System.out.println("================> Hello from OrdersBusinessService.test()");
    }



	@Override
	public List<Order> getOrders() {
		
		return orders;
	}



	@Override
	public void setOrders(List<Order> orders) {
				
	}

}
